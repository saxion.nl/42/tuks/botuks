## STUKS - Frontend
This is the project for the frontend part of the new STUKS Shop.

### To run and test the project
First install [Poetry](https://python-poetry.org/) if not already installed.

**When installed, do the following:**

1. `poetry install`, installs all necessary packages and what not to run the application.
2. `poetry shell`, enters the virtualenv in which the application can be started.
3. `flask run --reload`, this should start the Flask application and give you an address it runs on.

---

### To contribute to the project
To contribute to the frontend project of the STUKS Shop, you need to:
1. Have some experience with Python.
2. Have some experience with Flask.
3. Have some experience with Python [Poetry](https://python-poetry.org/)
4. Have some experience with TailwindCSS.

**To start developing, you need to do the following:**
1. Git clone this project.
2. Git clone the [backend project](https://gitlab.com/saxion.nl/42/tuks/the-meta-tuks).
3. Have experience with Docker and also have it installed, otherwise you're unable to run the backend part.
4. Open at least 2 terminal windows in the frontend project folder (wherever you've cloned it to).
5. In the first window run `poetry install`.
6. After the poetry install, in the first window first run `poetry shell` and then run `flask run --reload`. **Keep this window open!**
7. In the second window, run
`npx tailwindcss -i ./static/src/input.css -o ./static/dist/output.css --watch`.
**Also keep this window open!**

If you've followed the above steps, and nothing errored out, you should be able to develop and see your changes.

Tip! If you want to do fast responsive frontend development, download and install [ResponsivelyApp](https://responsively.app/). This is a tool that shows your webpage on multiple screen sizes at once. This will improve responsive web development time.

---

Happy coding!

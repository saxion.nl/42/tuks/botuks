from flask import Flask, render_template, request, redirect, session, flash, send_file, jsonify
from dotenv import load_dotenv
from datetime import datetime, timedelta
import functools
import requests
import jwt
import os
from auditor import auditor

from deep_translator import GoogleTranslator


load_dotenv()

TIME_DIFF = 2

SECRET_KEY = os.getenv("SECRET_KEY", "")
SECRET_ALGORITHM = os.getenv("SECRET_ALGORITHM", "")

PURCHASE_SECRET_KEY = os.getenv("PURCHASE_SECRET_KEY", "")

SECRET_ALGORITHMS = [SECRET_ALGORITHM]

API_URL = "http://127.0.0.1:8000"

app = Flask(__name__)
app.secret_key = "gcf1pak5PHQ6twh@yha"
app.config["TEMPLATES_AUTO_RELOAD"] = True
app.permanent_session_lifetime = timedelta(days=365)


def token_check():
    def decorator(func):
        @functools.wraps(func)
        def decorated_auth_check(*args, **kwargs):
            if not session.get('token'):
                session.clear()
                return redirect('/login')
            try:
                jwt.decode(session['token'], SECRET_KEY, SECRET_ALGORITHMS)
            except jwt.ExpiredSignatureError:
                session.clear()
                return redirect('/login')
            return func(*args, **kwargs)
        return decorated_auth_check
    return decorator

def auths_check(auth_roles: list):
    def decorator(func):
        @functools.wraps(func)
        def decorated_auth_check(*args, **kwargs):
            for role in auth_roles:
                if role in session['roles']:
                    return func(*args, **kwargs)
            return redirect('/')
        return decorated_auth_check
    return decorator

def auth_check(auth_role):
    def decorator(func):
        @functools.wraps(func)
        def decorated_auth_check(*args, **kwargs):
            if auth_role not in session['roles']:
                return redirect('/')
            return func(*args, **kwargs)
        return decorated_auth_check
    return decorator


@app.route('/manifest.json')
def serve_manifest():
    return send_file('manifest.json', mimetype='application/manifest+json')


# TODO: create 404 page.
# @app.errorhandler(404)
# def page_not_found(e):
#     return render_template('404.html'), 404


@app.route('/login', methods=['GET', 'POST'])
def login():
    if session.get('token'):
        return redirect('/')

    message = ''

    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        
        api_request = requests.post(f'{API_URL}/token', data={'username': username, 'password': password})

        if api_request.status_code == 200:
            session['token'] = api_request.json()['access_token']
            session.permanent = True
            decoded_session = jwt.decode(session['token'], SECRET_KEY, SECRET_ALGORITHMS)
            session['id'] = decoded_session['id']
            session['name'] = decoded_session['name']
            session['email'] = decoded_session['email']
            session['roles'] = decoded_session['roles']
            session['discount'] = decoded_session['discount']
            return redirect('/')
        else :
            message = "Incorrect credentials. Try Again!"

    return render_template('login.html', message=message)


@app.route('/logout')
# @token_check()
def logout():
    session.clear()
    return redirect('/')

@app.get('/reset-password/<token>')
def change_reset_password(token: str):
    return render_template('change-password.html', token=token)
    

@app.post('/reset-password/<token>')
def process_change_reset_password(token: str):
    password = request.form['password']
    repeat = request.form['repeat']
    if password != repeat:
        flash("Passwords doesn't match, try again!")
        return redirect(f'/reset-password/{token}')
    requests.post(
        f'{API_URL}/tokenreset-password/{token}',
        headers={
            'accept': 'application/json',
            'Content-Type': 'application/json'
        },
        json={
            "password": password
        })
    

    session.clear()
    return redirect('/login')

@app.get('/reset-password')
def reset_password():
    return render_template('reset-password.html')


@app.post('/reset-password')
def request_reset_password():
    email = request.form['email']
    response = requests.post(
        f'{API_URL}/tokenreset-password',
        headers={
            'accept': 'application/json',
            'Content-Type': 'application/json'
        },
        json={
            "email": str(email)
        })
    
    if response.status_code == 422:
        flash("Email address don't exist, try again.")
        return redirect('/reset-password')
    
    return redirect('/')


# MAIN ROUTES FROM TAB BAR
@app.route('/')
@token_check()
def index():
    session['cart'] = []
    # TODO: change this when we know what to do with the home page.
    # return render_template('index.html', page_title="Home - don't know if necessary")
    return redirect('/shop')


@app.route('/shop')
@token_check()
def shop():
    products = requests.get(
        f'{API_URL}/products',
        headers={
            'Authorization': f"Bearer {session['token']}",
        }
    ).json()

    return render_template('shop.html', page_title='Shop', products=products)


@app.get('/change-stock')
@token_check()
@auths_check(['board', 'fridge'])
def change_stock():
    products = requests.get(
        f'{API_URL}/products',
        headers={
            'Authorization': f"Bearer {session['token']}",
        }
    ).json()
    return render_template('stock-update.html', page_title="Stock update", products=products)


@app.post('/change-stock')
@token_check()
@auths_check(['board', 'fridge'])
def change_stock_process():
    # TODO Implement the API call to change the stock
    id = int(request.form['id'])
    amount = int(request.form['amount'])
    print(f'id: {id} ({type(id)}) and amount: {amount} ({type(amount)})')
    result = requests.patch(
        f'{API_URL}/products/update-stock',
        headers={
            'Authorization': f"Bearer {session['token']}",
        },
        json={
            'id': id,
            'amount': amount
        }
    )
    flash(f'{result.json()['detail']}')
    return redirect('/change-stock')


@app.route('/history')
@token_check()
def history():
    user_history = {}
    history = requests.get(
        f"{API_URL}/me/purchase",
        headers={
            'Authorization': f"Bearer {session['token']}",
        }
    ).json()
    
    if 'status' not in history:
        for purchase in history:
            date_object = datetime.strptime(purchase['date'], "%Y-%m-%dT%H:%M:%S") + timedelta(hours=TIME_DIFF) # Added 2 hours for the server difference
            purchase_date = date_object.strftime('%d-%m-%Y')
            product = requests.get(f"{API_URL}/products/{purchase['product_id']}").json()
            product['price'] = purchase['price']
            product['time'] = date_object.strftime('%H:%M')
            if purchase_date not in user_history:
                user_history[purchase_date] = {}
                user_history[purchase_date]['products'] = [product]
                user_history[purchase_date]['total_cost'] = product['price']
            else:
                user_history[purchase_date]['products'].append(product)
                user_history[purchase_date]['total_cost'] += product['price']
    
    return render_template('user-history.html', page_title='History', user_history=user_history)


@app.route('/me')
@token_check()
def get_user():
    user_history = {}
    user = requests.get(
        f"{API_URL}/me",
        headers={
            'Authorization': f"Bearer {session['token']}",
        }
    ).json()
    history = requests.get(
        f"{API_URL}/me/purchase",
        headers={
            'Authorization': f"Bearer {session['token']}",
        }
    ).json()
    
    if 'status' not in history:
        for purchase in history:
            date_object = datetime.strptime(purchase['date'], "%Y-%m-%dT%H:%M:%S") + timedelta(hours=TIME_DIFF) # Added 2 hours for the server difference
            purchase_date = date_object.strftime('%d-%m-%Y')
            product = requests.get(f"{API_URL}/products/{purchase['product_id']}").json()
            if purchase_date not in user_history:
                user_history[purchase_date] = {}
                user_history[purchase_date]['products'] = [product]
                user_history[purchase_date]['total_cost'] = round((product['price'] * session['discount']), 2)
            else:
                user_history[purchase_date]['products'].append(product)
                user_history[purchase_date]['total_cost'] += round((product['price'] * session['discount']), 2)

    return render_template('user.html', page_title=f"{user['name']}", user=user, user_history=user_history)


@app.route('/admin')
@token_check()
@auth_check('board')
def admin():

    return render_template('admin.html', page_title='Admin')


# COMPLEMENTARY ROUTES FOR (SUPPORTING) FUNCTIONS
@app.route('/cart')
@token_check()
def cart():

    products = []
    total_cost = 0

    for product_id in session['cart']:
        product = requests.get(f"{API_URL}/products/{product_id}").json()
        total_cost += round((product['price'] * session['discount']), 2)
        products.append(product)

    return render_template('cart.html', page_title='Shopping Cart', products=products, total_cost=total_cost)


def is_halal(product):
    return 'Yes' if 'halal' in (product.get('labels', '')).lower() or 'halal' in (product.get('ingredients_text', '')).lower() else 'No'

def is_veggie(product):
    return 'Yes' if 'vegetarian' in (product.get('labels', '')).lower() or 'vegetarian' in (product.get('ingredients_text', '')).lower() else 'No'

def capitalize_allergen(allergen):
    if ":" in allergen:
        _lang, substance = allergen.split(":")
        return f"{substance.capitalize()}"
    else:
        return f"{allergen.capitalize()}"

def translate_to_english(text):
    try:
        return GoogleTranslator(source='auto', target='en').translate(text)
    except Exception as e:
        return f"Translation failed: {e}"
    
def fetch_json(url, params=None, headers=None):
        response = requests.get(url, params=params, headers=headers)
        return response.json()

@app.route('/info/<product_id>')
@token_check()
def info(product_id):
    def get_product_details(product_data):
        allergens = product_data.get('allergens', 'No allergens listed')
        nutriscore = product_data.get('nutriscore_grade', 'No nutriscore found.')
        formatted_allergens = (
            ", ".join([translate_to_english(capitalize_allergen(allergen)) 
                       for allergen in allergens.split(",")])
            if allergens != 'No allergens listed' else allergens
        )
        ingredients = translate_to_english(product_data.get('ingredients_text', 'Not available'))

        return {
            'brand': product_data.get('brands', 'Unknown'),
            'ingredients': ingredients,
            'image_url': product_data.get('image_url', ''),
            'allergens': formatted_allergens,
            'nutriscore': nutriscore,
            'halal': is_halal(product_data),
            'vegetarian': is_veggie(product_data),
        }

    user = fetch_json(
        f"{API_URL}/me",
        headers={'Authorization': f"Bearer {session['token']}"}
    )

    # Fetch product details from your API/database using the product ID
    product = fetch_json(f"{API_URL}/products/{product_id}")
    product_name = product.get('name', '')
    ean_code = product.get('ean_code', '')

    # Use OpenFoodFacts V2 API
    if ean_code:
        openfoodfacts_data = fetch_json(
            f"https://world.openfoodfacts.org/api/v2/product/{ean_code}"
        )
        off_product = openfoodfacts_data.get('product', None)

        if off_product:
            off_details = get_product_details(off_product)
        else:
            off_details = {
                'brand': 'Unknown',
                'ingredients': 'Not available',
                'image_url': '',
                'nutriscore': "Unknown",
                'allergens': 'No allergens listed',
                'halal': 'Unknown',
                'vegetarian': 'Unknown',
            }
    else:
        off_details = {
            'brand': 'Unknown',
            'ingredients': 'Not available',
            'image_url': '',
            'nutriscore': "Unknown",
            'allergens': 'No allergens listed',
            'halal': 'Unknown',
            'vegetarian': 'Unknown',
        }

    return render_template(
        'info.html',
        page_title='Product Information',
        user=user,
        product=product,
        openfoodfacts=off_details
    )



@app.route('/cart/clear')
@token_check()
def clear_cart():
    session['cart'] = []
    return redirect('/cart')


@app.route('/cart/purchase')
@token_check()
def purchase():
    purchases_made = []
    for product_id in session['cart']:
        response = requests.post(
            f"{API_URL}/me/purchase?product_id={product_id}",
            headers={
                'Authorization': f"Bearer {session['token']}",
                'accept': 'application/json',
            },
        )
        auditor(f"User {session['name']} (id: {session['id']}) bought product {product_id}, response API: {response.status_code}", "BUY")
        purchases_made.append(response.json())
    if len(purchases_made) != 0:
        session['cart'] = []
        return redirect('/shop')
    return redirect('/cart')


@app.route("/cart/<product_id>", methods=['POST'])
@token_check()
def add_to_cart(product_id):
    if 'cart' not in session:
        session['cart'] = []

    session['cart'] += [product_id]

    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':

        return jsonify({
            'message': 'Product added to cart.',
            'cart_count': len(session['cart'])
        })
    
    return redirect('/shop')


@app.route('/cart/<product_id>/remove')
@token_check()
def remove_from_cart(product_id):
    cart_list = session['cart']
    cart_list.remove(product_id)
    session['cart'] = cart_list
    return redirect('/cart')


@app.route('/me/profile-picture', methods=['GET', 'POST'])
def profile_picture():
    if request.method == 'POST':
        file = request.files['profile-picture']
        
        requests.patch(
                f"{API_URL}/me",
                headers={
                    'Authorization': f"Bearer {session['token']}",
                    'accept': 'application/json',
                },
                files = {'file': (file.filename, file.stream, file.content_type)}
            )
            
    return redirect('/me')


@app.route('/admin/products')
@token_check()
@auth_check('board')
def admin_products():
    products = requests.get(
        f"{API_URL}/products/all",
        headers={
            'Authorization': f"Bearer {session['token']}"
        }
    ).json()

    return render_template('products.html', page_title='Edit Products', products=products)


@app.route('/admin/products/new', methods=['GET', 'POST'])
@token_check()
@auth_check('board')
def admin_new_product():
    if request.method != 'POST':
        return render_template('product-new.html', page_title='New Product')
    
    response = requests.post(
        f"{API_URL}/products",
        headers={
            'Authorization': f"Bearer {session['token']}",
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        json={
            'name': request.form['product-name'],
            'price': float(request.form['product-price']),
            'is_active': request.form['product-active'],
            'ean_code': request.form.get("product-ean", None),
            'type': request.form['product-type'],
            'prohibited': request.form.get('product-prohibited', None) == 'on'
        }
    )
    if response.status_code != 200:
        return redirect('/admin/products')
    
    return redirect(f'/admin/products/{response.json()['id']}')


@app.route('/admin/products/<int:product_id>', methods=['GET', 'POST'])
@token_check()
@auth_check('board')
def admin_edit_product(product_id: int):
    product = requests.get(f"{API_URL}/products/{product_id}").json()

    if request.method == 'POST':
        product['name'] = request.form['product-name']
        product['price'] = float(request.form['product-price'])
        product['stock'] = request.form['product-stock']
        product['type'] = request.form['product-type']
        product['prohibited'] = request.form.get('product-prohibited', None) == 'on'
        product['ean_code'] = request.form.get("product-ean", None)
        
        requests.put(
            f"{API_URL}/products/{product['id']}",
            headers={
                'Authorization': f"Bearer {session['token']}",
                'accept': 'application/json',
            },
            json=product,
        )
        if request.form['product-active'] != product['is_active']:
            match request.form['product-active']:
                case 'False':
                    requests.patch(
                        f"{API_URL}/products/{product['id']}/disable",
                        headers={
                            'Authorization': f"Bearer {session['token']}",
                            'accept': 'application/json',
                        }
                    )
                case 'True':
                    requests.patch(
                        f"{API_URL}/products/{product['id']}/enable",
                        headers={
                            'Authorization': f"Bearer {session['token']}",
                            'accept': 'application/json',
                        }
                    )
                case _:
                    pass
        
        return redirect('/admin/products')
    
    return render_template('product-edit.html', page_title=product['name'], product=product)


@app.route('/admin/products/<int:product_id>/picture', methods=['GET', 'POST'])
@token_check()
@auth_check("board")
def admin_product_picture(product_id: int):
    if request.method == 'POST':
        file = request.files['product-picture']
        requests.post(
                f"{API_URL}/products/{product_id}/picture",
                headers={
                    'Authorization': f"Bearer {session['token']}",
                    'accept': 'application/json',
                },
                files = {'file': (file.filename, file.stream, file.content_type)}
            )
            
    return redirect('/admin/products')


@app.route('/admin/users')
@token_check()
@auth_check('board')
def admin_users():
    users = requests.get(
        f"{API_URL}/users/all",
        headers={
            'Authorization': f"Bearer {session['token']}",
            'accept': 'application/json',
        }
    ).json()

    return render_template('users.html', page_title='Edit Users', users=users)


@app.route('/admin/users/<user_id>/edit', methods=['GET', 'POST'])
@token_check()
@auth_check('board')
def admin_user_edit(user_id):
    user = requests.get(
        f"{API_URL}/users/{user_id}",
        headers={
            'Authorization': f"Bearer {session['token']}",
            'accept': 'application/json',
        }
    ).json()

    roles = requests.get(f"{API_URL}/users/roles").json()

    if request.method == 'POST':
        user['name'] = request.form['user-name']
        user['email'] = request.form['user-email']
        user['allowed_prohibited'] = request.form.get('user-prohibited', False)
        

        requests.put(
            f"{API_URL}/users/{user['id']}",
            headers={
                'Authorization': f"Bearer {session['token']}",
                'accept': 'application/json',
            },
            json=user,
        )
        
        if request.form['user-active'] != user['is_active']:
            match request.form['user-active']:
                case 'False':
                    requests.patch(
                        f"{API_URL}/users/{user['id']}/inactive",
                        headers={
                            'Authorization': f"Bearer {session['token']}",
                            'accept': 'application/json',
                        }
                    )
                case 'True':
                    requests.patch(
                        f"{API_URL}/users/{user['id']}/active",
                        headers={
                            'Authorization': f"Bearer {session['token']}",
                            'accept': 'application/json',
                        }
                    )
                case _:
                    pass

        if request.form['user-moneybird']:
            requests.patch(
                f"{API_URL}/users/{user_id}/moneybird?moneybird_id={request.form['user-moneybird']}",
                headers={
                    'Authorization': f"Bearer {session['token']}",
                    'accept': 'application/json'
                }
            )
        
        for role in roles:
            if role in request.form and user['roles'] and role in user['roles']:
                continue
            if role not in request.form and user['roles'] and role not in user['roles']:
                continue

            # Role request but not assigned to selected user
            if role in request.form and user['roles'] and role not in user['roles']:
                requests.patch(
                    f"{API_URL}/users/{user['id']}/role?role={role}",
                    headers={
                        'Authorization': f"Bearer {session['token']}",
                        'accept': 'application/json',
                    }
                )
                continue
            # Role is deselected but has the role now.
            if role not in request.form and user['roles'] and role in user['roles']:
                requests.delete(
                    f"{API_URL}/users/{user['id']}/role?role={role}",
                    headers={
                        'Authorization': f"Bearer {session['token']}",
                        'accept': 'application/json',
                    }
                )
        
        return redirect(f'/admin/users')

    return render_template('user-edit.html', page_title=f"Edit {user['name']}", user=user, roles=roles)


@app.route('/admin/users/<user_id>/password-reset')
@token_check()
@auth_check('board')
def admin_user_password_reset(user_id):
    user = requests.get(
        f"{API_URL}/users/{user_id}",
        headers={
            'Authorization': f"Bearer {session['token']}",
            'accept': 'application/json',
        }
    ).json()
    requests.post(
        f"{API_URL}/tokenreset-password",
        headers={
            'Authorization': f"Bearer {session['token']}",
            'accept': 'application/json',
            'Content-Type': 'application/json',
        },
        json={
            'email': user['email']
        }
    )
    return redirect('/admin/users')


@app.route('/admin/users/new-user', methods=['GET', 'POST'])
@token_check()
@auth_check('board')
def admin_new_user():
    if request.method != 'POST':
        return render_template('user-new.html', page_title='New User')
    
    requests.post(
        f"{API_URL}/users",
        headers={
            'Authorization': f"Bearer {session['token']}",
            'accept': 'application/json',
            'Content-Type': 'application/json',
        },
        json={
            'name': request.form['user-name'],
            'email': request.form['user-email'],
            'password': request.form['user-password'],
            'moneybird_id': request.form['user-moneybird']
        }
    )
    return redirect('/admin/users')


@app.route("/purchases")
@token_check()
def login_purchase_history():
    return purchase_history()


@app.route("/purchases-tv")
def cookie_purchase_history():
    if request.cookies.get("purchase_key") == PURCHASE_SECRET_KEY:
        return purchase_history()
    return redirect("/login")


def purchase_history():
    purchase_history = {}
    history = requests.get(f"{API_URL}/purchases").json()

    if 'status' not in history:
        for purchase in history:
            date_object = datetime.strptime(purchase['date'], "%Y-%m-%dT%H:%M:%S") + timedelta(hours=TIME_DIFF) # Added 2 hours for the server difference
            purchase_date = date_object.strftime('%d-%m-%Y')
            time_object = date_object.strftime('%H:%M')
            purchase['time'] = time_object

            if purchase_date not in purchase_history:
                purchase_history[purchase_date] = [purchase]
                
            else:
                purchase_history[purchase_date].append(purchase)
    
    return render_template('purchase-history.html', page_title='Purchase history', purchase_history=purchase_history)

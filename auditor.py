from datetime import datetime
AUDIT_LOG = 'audit.log'


def auditor(msg: str, category: str | None = None):
    with open(AUDIT_LOG, 'a') as writer:
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        line = f"{timestamp} AUDITOR"
        if category:
            line += f":[{category}]"
        line += ":" + str(msg)
        writer.write(line)
        writer.write('\n')

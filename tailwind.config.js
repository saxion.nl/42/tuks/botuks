/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./templates/**/*.html"
  ],
  theme: {
    extend: {},
    colors: {
      'yellow': '#EBD02C',
      'white': '#FFFFFF',
      'red': "#EB402C",
      'grey': '#2F2F2F',
      'grey-dark': '#1E1E1E',
      'grey-light': '#3C3C3C',
    },
    fontFamily: {
      'koulen': ["Koulen", "sans-serif"],
      'anton': ["Anton", "sans-serif"],
    },
    container: {
      center: true,
    }
  }
}